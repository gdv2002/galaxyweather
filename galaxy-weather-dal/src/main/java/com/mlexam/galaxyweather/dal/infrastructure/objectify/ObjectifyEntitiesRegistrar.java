package com.mlexam.galaxyweather.dal.infrastructure.objectify;

import com.googlecode.objectify.ObjectifyService;
import com.mlexam.galaxyweather.model.entity.Planet;
import com.mlexam.galaxyweather.model.entity.PlanetState;
import com.mlexam.galaxyweather.model.entity.SystemDescription;
import com.mlexam.galaxyweather.model.entity.WeatherCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObjectifyEntitiesRegistrar {

    private static Logger logger = LoggerFactory.getLogger(ObjectifyEntitiesRegistrar.class);

    public static void register() {
        logger.info("Registering objectify entities...");

        ObjectifyService.register(Planet.class);
        ObjectifyService.register(PlanetState.class);
        ObjectifyService.register(SystemDescription.class);
        ObjectifyService.register(WeatherCondition.class);
    }
}
