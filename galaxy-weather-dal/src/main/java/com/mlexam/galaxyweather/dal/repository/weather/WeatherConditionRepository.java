package com.mlexam.galaxyweather.dal.repository.weather;

import com.mlexam.galaxyweather.dal.repository.Repository;
import com.mlexam.galaxyweather.model.entity.WeatherCondition;

import java.util.List;

public interface WeatherConditionRepository extends Repository<WeatherCondition> {

    WeatherCondition getLastDay();

    List<WeatherCondition> getBetweenDays(int fromDayNonInclusive, int toDayInclusive);

    WeatherCondition getByDay(int day);
}
