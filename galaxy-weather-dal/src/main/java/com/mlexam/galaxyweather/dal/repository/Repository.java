package com.mlexam.galaxyweather.dal.repository;

import com.mlexam.galaxyweather.model.entity.BaseEntity;

public interface Repository<EntityType extends BaseEntity> {

    EntityType getById(String id);

    EntityType getById(String id, Object parentKeyOrEntity);

    EntityType save(EntityType entity);

    void delete(EntityType entity);
}
