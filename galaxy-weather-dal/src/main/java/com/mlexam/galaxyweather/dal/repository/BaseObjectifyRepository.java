package com.mlexam.galaxyweather.dal.repository;

import com.googlecode.objectify.cmd.LoadType;
import com.mlexam.galaxyweather.model.entity.BaseEntity;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.ParameterizedType;

import static com.googlecode.objectify.ObjectifyService.ofy;

public abstract class BaseObjectifyRepository<EntityType extends BaseEntity> implements Repository<EntityType> {

    // Representa el tipo EntityType especificado
    private final Class<EntityType> genericClass;

    @SuppressWarnings("unchecked")
    public BaseObjectifyRepository() {
        this.genericClass = (Class<EntityType>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected LoadType<EntityType> getLoadType() {
        return ofy().load().type(genericClass);
    }

    @Override
    public EntityType getById(String id) {
        return getLoadType().id(id).now();
    }

    @Override
    public EntityType getById(String id, Object parentKeyOrEntity) {
        return getLoadType().parent(parentKeyOrEntity).id(id).now();
    }

    @Override
    public EntityType save(EntityType entity) {

        if (StringUtils.isBlank(entity.getId()))
            entity.assignNewRandomId();

        ofy().save().entity(entity);

        return entity;
    }

    @Override
    public void delete(EntityType entity) {
        ofy().delete().entity(entity);
    }
}
