package com.mlexam.galaxyweather.dal.infrastructure.ioc;

import com.mlexam.galaxyweather.dal.infrastructure.objectify.ObjectifyEntitiesRegistrar;
import com.mlexam.galaxyweather.dal.repository.BaseObjectifyRepository;
import com.mlexam.galaxyweather.dal.repository.Repository;
import com.mlexam.galaxyweather.dal.repository.weather.WeatherConditionRepository;
import com.mlexam.galaxyweather.dal.repository.weather.WeatherConditionObjectifyRepositoryImpl;
import com.mlexam.galaxyweather.model.entity.Planet;
import com.mlexam.galaxyweather.model.entity.PlanetState;
import com.mlexam.galaxyweather.model.entity.SystemDescription;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SuppressWarnings("unused")
@Configuration
public class DalModuleConfiguration {

    static {
        ObjectifyEntitiesRegistrar.register();
    }

    @Bean
    public Repository<Planet> planetRepository() {
        return new BaseObjectifyRepository<Planet>() {
        };
    }

    @Bean
    public Repository<PlanetState> planetStateRepository() {
        return new BaseObjectifyRepository<PlanetState>() {
        };
    }

    @Bean
    public Repository<SystemDescription> systemDescriptionRepository() {
        return new BaseObjectifyRepository<SystemDescription>() {
        };
    }

    @Bean
    public WeatherConditionRepository weatherConditionRepository() {
        return new WeatherConditionObjectifyRepositoryImpl();
    }
}
