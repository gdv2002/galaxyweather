package com.mlexam.galaxyweather.dal.repository.weather;

import com.google.appengine.api.datastore.KeyFactory;
import com.mlexam.galaxyweather.dal.repository.BaseObjectifyRepository;
import com.mlexam.galaxyweather.model.entity.WeatherCondition;

import java.util.List;

public class WeatherConditionObjectifyRepositoryImpl extends BaseObjectifyRepository<WeatherCondition> implements WeatherConditionRepository {

    public static final String KIND = "WeatherCondition";
    public static final String SHARED_KEY = "sharedkey";

    @Override
    public WeatherCondition getLastDay() {
        List<WeatherCondition> lastDays = getLoadType().order("-day")
                .ancestor(KeyFactory.createKey(KIND, SHARED_KEY))
                .limit(1).list();
        return returnSingle(lastDays);
    }

    @Override
    public List<WeatherCondition> getBetweenDays(int fromDayNonInclusive, int toDayInclusive) {
        return getLoadType().filter("day >", fromDayNonInclusive).filter("day <=", toDayInclusive)
                .ancestor(KeyFactory.createKey(KIND, SHARED_KEY))
                .order("day")
                .list();
    }

    @Override
    public WeatherCondition getByDay(int day) {
        List<WeatherCondition> days = getLoadType().filter("day", day)
                .ancestor(KeyFactory.createKey(KIND, SHARED_KEY))
                .list();
        return returnSingle(days);
    }

    @Override
    public WeatherCondition save(WeatherCondition entity) {
        entity.assignSharedParent(KIND, SHARED_KEY);
        return super.save(entity);
    }

    private WeatherCondition returnSingle(List<WeatherCondition> list) {
        if (list.isEmpty())
            return null;
        return list.get(0);
    }
}
