package com.mlexam.galaxyweather.business.geometry;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TrigonometryHelperTest {

    private TrigonometryHelper trigonometryHelper;

    @Before
    public void setup() {
        trigonometryHelper = new TrigonometryHelper();
    }

    @Test
    public void triangleShouldContainOriginWhenItIsInside() {
        Point vertA = new Point(-20, -20);
        Point vertB = new Point(-2, 50);
        Point vertC = new Point(30, 0);
        TriangleShapeInfo triangleShapeInfo = trigonometryHelper.analyzeTriangle(vertA, vertB, vertC);

        assertEquals(true, triangleShapeInfo.isOriginContained());
    }

    @Test
    public void triangleShouldContainOriginWhenItIsOverAnEdge() {
        Point vertA = new Point(-20, -20);
        Point vertB = new Point(-2, 50);
        Point vertC = new Point(20, 20);
        TriangleShapeInfo triangleShapeInfo = trigonometryHelper.analyzeTriangle(vertA, vertB, vertC);

        assertEquals(true, triangleShapeInfo.isOriginContained());
    }

    @Test
    public void triangleShouldNotContainOriginWhenItIsNotInside() {
        Point vertA = new Point(-80, -20);
        Point vertB = new Point(-0.1, 50);
        Point vertC = new Point(-0.1, -3);
        TriangleShapeInfo triangleShapeInfo = trigonometryHelper.analyzeTriangle(vertA, vertB, vertC);

        assertEquals(false, triangleShapeInfo.isOriginContained());
    }

    @Test
    public void triangleShouldNotBeALine() {
        Point vertA = new Point(-20, -20);
        Point vertB = new Point(-2, 50);
        Point vertC = new Point(30, 0);
        TriangleShapeInfo triangleShapeInfo = trigonometryHelper.analyzeTriangle(vertA, vertB, vertC);

        assertEquals(false, triangleShapeInfo.getArea().equals(BigDecimal.ZERO));
    }

    @Test
    public void shapeShouldBeALine() {
        Point vertA = new Point(-40, -40);
        Point vertB = new Point(-10, 0);
        Point vertC = new Point(50, 80);
        TriangleShapeInfo triangleShapeInfo = trigonometryHelper.analyzeTriangle(vertA, vertB, vertC);

        assertEquals(true, triangleShapeInfo.getArea().equals(BigDecimal.ZERO));
    }

    @Test
    public void lineShouldContainOriginWhenItIsOver() {
        Point vertA = new Point(-40, 10);
        Point vertB = new Point(-10, 2.5);
        Point vertC = new Point(20, -5);
        TriangleShapeInfo triangleShapeInfo = trigonometryHelper.analyzeTriangle(vertA, vertB, vertC);

        assertEquals(true, triangleShapeInfo.getArea().equals(BigDecimal.ZERO));
        assertEquals(true, triangleShapeInfo.isOriginContained());
    }

    @Test
    public void lineShouldContainOriginWhenAnExtensionOfItWouldBeOver() {
        Point vertA = new Point(-40, 10);
        Point vertB = new Point(-10, 2.5);
        Point vertC = new Point(-20, 5);
        TriangleShapeInfo triangleShapeInfo = trigonometryHelper.analyzeTriangle(vertA, vertB, vertC);

        assertEquals(true, triangleShapeInfo.getArea().equals(BigDecimal.ZERO));
        assertEquals(true, triangleShapeInfo.isOriginContained());
    }

    @Test
    public void lineShouldNotContainOrigin() {
        Point vertA = new Point(-40, -40);
        Point vertB = new Point(-10, 0);
        Point vertC = new Point(50, 80);
        TriangleShapeInfo triangleShapeInfo = trigonometryHelper.analyzeTriangle(vertA, vertB, vertC);

        assertEquals(true, triangleShapeInfo.getArea().equals(BigDecimal.ZERO));
        assertEquals(false, triangleShapeInfo.isOriginContained());
    }

    @Test
    public void shouldGetThePointFromRadiusAndDegrees() {
        Point point = trigonometryHelper.getPoint(100d, 0d);
        assertEquals(new Point(100, 0), point);
        point = trigonometryHelper.getPoint(100d, 90d);
        assertEquals(new Point(0, 100), point);
        point = trigonometryHelper.getPoint(100d, 180d);
        assertEquals(new Point(-100, 0), point);
        point = trigonometryHelper.getPoint(100d, 270d);
        assertEquals(new Point(0, -100), point);

        point = trigonometryHelper.getPoint(500d, 30d);
        assertEquals(433.01d, point.getX(), 0.01d);
        assertEquals(250d, point.getY(), 0d);

        point = trigonometryHelper.getPoint(500d, 150d);
        assertEquals(-433.01d, point.getX(), 0.01d);
        assertEquals(250d, point.getY(), 0d);

        point = trigonometryHelper.getPoint(500d, 210d);
        assertEquals(-433.01d, point.getX(), 0.01d);
        assertEquals(-250d, point.getY(), 0d);

        point = trigonometryHelper.getPoint(500d, 300d);
        assertEquals(250d, point.getX(), 0d);
        assertEquals(-433.01d, point.getY(), 0.01d);
    }
}