package com.mlexam.galaxyweather.business.simulation;

import com.mlexam.galaxyweather.business.geometry.TriangleShapeInfo;
import com.mlexam.galaxyweather.business.meteorology.GalaxyWeatherSummary;
import com.mlexam.galaxyweather.business.meteorology.WeatherAnalyzer;
import com.mlexam.galaxyweather.business.meteorology.WeatherKind;
import com.mlexam.galaxyweather.business.space.mechanics.MovablePlanet;
import com.mlexam.galaxyweather.model.entity.WeatherCondition;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class WeatherPredictorImplTest {

    @Mock
    private WeatherAnalyzer weatherAnalyzer;

    @InjectMocks
    private WeatherPredictor weatherPredictor;

    @Before
    public void setup() {
        weatherPredictor = new WeatherPredictorImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldPredictDesiredDaysFromNow() {

        when(weatherAnalyzer.analyzeSystem(any(TriangleShapeInfo.class), any(TriangleShapeInfo.class)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.UNDETERMINED, null));

        int days = 20;
        MovablePlanet planet1 = new MovablePlanet(100d, 45d, 1d);
        MovablePlanet planet2 = new MovablePlanet(200d, 45d, 1d);
        MovablePlanet planet3 = new MovablePlanet(300d, 45d, 1d);

        GalaxyWeatherPrediction prediction = weatherPredictor.predictBehavior(planet1, planet2, planet3, days, null);

        assertEquals(days, prediction.getDayPredictionDetails().size());
    }

    @Test
    public void shouldSummarizeThePredictionBasedOnWeatherKind() {

        when(weatherAnalyzer.analyzeSystem(any(TriangleShapeInfo.class), any(TriangleShapeInfo.class)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, BigDecimal.ONE))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.OPTIMAL, null))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, BigDecimal.ONE))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.DRY, null))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.OPTIMAL, null))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.UNDETERMINED, null));

        int days = 6;
        MovablePlanet planet1 = new MovablePlanet(100d, 45d, 1d);
        MovablePlanet planet2 = new MovablePlanet(200d, 45d, 1d);
        MovablePlanet planet3 = new MovablePlanet(300d, 45d, 1d);

        GalaxyWeatherPrediction prediction = weatherPredictor.predictBehavior(planet1, planet2, planet3, days, null);

        assertEquals(1, prediction.getDryDaysCount());
        assertEquals(2, prediction.getRainyDaysCount());
        assertEquals(2, prediction.getOptimalWeatherDaysCount());
    }

    @Test
    public void shouldInformTheRainiestDays() {

        when(weatherAnalyzer.analyzeSystem(any(TriangleShapeInfo.class), any(TriangleShapeInfo.class)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(600d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.OPTIMAL, null))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(800d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(500d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(800d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.OPTIMAL, null))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.UNDETERMINED, null));

        int days = 10;
        MovablePlanet planet1 = new MovablePlanet(100d, 45d, 1d);
        MovablePlanet planet2 = new MovablePlanet(200d, 45d, 1d);
        MovablePlanet planet3 = new MovablePlanet(300d, 45d, 1d);

        GalaxyWeatherPrediction prediction = weatherPredictor.predictBehavior(planet1, planet2, planet3, days, null);

        assertEquals(new BigDecimal(800d), prediction.getMaxRainyDayPerimeter());
        assertEquals(2, prediction.getTopRainyDays().size());
        assertEquals(3, prediction.getTopRainyDays().get(0).intValue());
        assertEquals(5, prediction.getTopRainyDays().get(1).intValue());
    }

    @Test
    public void shouldInformTheRainiestDaysCorrectlyWhenTheMaximumPerimeterIsKnownInAdvance() {

        when(weatherAnalyzer.analyzeSystem(any(TriangleShapeInfo.class), any(TriangleShapeInfo.class)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(600d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.OPTIMAL, null))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(800d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(500d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(800d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.OPTIMAL, null))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.UNDETERMINED, null));

        int days = 10;
        MovablePlanet planet1 = new MovablePlanet(100d, 45d, 1d);
        MovablePlanet planet2 = new MovablePlanet(200d, 45d, 1d);
        MovablePlanet planet3 = new MovablePlanet(300d, 45d, 1d);

        GalaxyWeatherPrediction prediction = weatherPredictor.predictBehavior(planet1, planet2, planet3, days, new BigDecimal(1000d));

        assertEquals(new BigDecimal(1000d), prediction.getMaxRainyDayPerimeter());
        assertEquals(0, prediction.getTopRainyDays().size());


        when(weatherAnalyzer.analyzeSystem(any(TriangleShapeInfo.class), any(TriangleShapeInfo.class)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(600d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.OPTIMAL, null))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(800d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(500d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.RAIN, new BigDecimal(800d)))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.OPTIMAL, null))
                .thenReturn(new GalaxyWeatherSummary(WeatherKind.UNDETERMINED, null));

        prediction = weatherPredictor.predictBehavior(planet1, planet2, planet3, days, new BigDecimal(800d));

        assertEquals(new BigDecimal(800d), prediction.getMaxRainyDayPerimeter());
        assertEquals(2, prediction.getTopRainyDays().size());
        assertEquals(3, prediction.getTopRainyDays().get(0).intValue());
        assertEquals(5, prediction.getTopRainyDays().get(1).intValue());
    }

    @Test
    public void shouldSummarizeTheSpecifiedPredictions() {

        List<WeatherCondition> weatherConditions = new ArrayList<>();
        weatherConditions.add(WeatherPredictorImplTestFixture.buildWeatherCondition(1, com.mlexam.galaxyweather.model.entity.WeatherKind.DRY, 0d));
        weatherConditions.add(WeatherPredictorImplTestFixture.buildWeatherCondition(2, com.mlexam.galaxyweather.model.entity.WeatherKind.RAIN, 39.999999d));
        weatherConditions.add(WeatherPredictorImplTestFixture.buildWeatherCondition(3, com.mlexam.galaxyweather.model.entity.WeatherKind.DRY, 0d));
        weatherConditions.add(WeatherPredictorImplTestFixture.buildWeatherCondition(4, com.mlexam.galaxyweather.model.entity.WeatherKind.RAIN, 38d));
        weatherConditions.add(WeatherPredictorImplTestFixture.buildWeatherCondition(5, com.mlexam.galaxyweather.model.entity.WeatherKind.DRY, 0d));
        weatherConditions.add(WeatherPredictorImplTestFixture.buildWeatherCondition(6, com.mlexam.galaxyweather.model.entity.WeatherKind.OPTIMAL, 0d));
        weatherConditions.add(WeatherPredictorImplTestFixture.buildWeatherCondition(7, com.mlexam.galaxyweather.model.entity.WeatherKind.UNDETERMINED, 20d));
        weatherConditions.add(WeatherPredictorImplTestFixture.buildWeatherCondition(8, com.mlexam.galaxyweather.model.entity.WeatherKind.RAIN, 40d));
        weatherConditions.add(WeatherPredictorImplTestFixture.buildWeatherCondition(9, com.mlexam.galaxyweather.model.entity.WeatherKind.RAIN, 21d));

        GalaxyWeatherPredictionSummary summary = weatherPredictor.summarizePrediction(weatherConditions, new BigDecimal(40d), 0);
        assertEquals(3, summary.getDryDaysCount());
        assertEquals(4, summary.getRainyDaysCount());
        assertEquals(1, summary.getOptimalWeatherDaysCount());
        assertEquals(2, summary.getTopRainyDays().size());
        assertEquals(2, summary.getTopRainyDays().get(0).intValue());
        assertEquals(8, summary.getTopRainyDays().get(1).intValue());
    }
}