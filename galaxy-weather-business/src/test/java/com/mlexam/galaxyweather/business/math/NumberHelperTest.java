package com.mlexam.galaxyweather.business.math;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class NumberHelperTest {

    @Test
    public void shouldConsiderEqualityWithTolerance() {

        boolean areEqual = NumberHelper.areEqual(new BigDecimal(1.0003d), new BigDecimal(1.0004d), new BigDecimal(0.0001d));
        assertEquals(true, areEqual);

        areEqual = NumberHelper.areEqual(new BigDecimal(1.0003d), new BigDecimal(1.0005d), new BigDecimal(0.0001d));
        assertEquals(false, areEqual);

        areEqual = NumberHelper.areEqual(new BigDecimal(-1.0003d), new BigDecimal(-1.0004d), new BigDecimal(0.0001d));
        assertEquals(true, areEqual);

        areEqual = NumberHelper.areEqual(new BigDecimal(-1.0003d), new BigDecimal(-1.0005d), new BigDecimal(0.0001d));
        assertEquals(false, areEqual);

        areEqual = NumberHelper.areEqual(new BigDecimal(-1.0003d), new BigDecimal(-1.000321d), new BigDecimal(0.0001d));
        assertEquals(true, areEqual);
    }

    @Test
    public void shouldGetTheRightMaxValue() {

        BigDecimal max = NumberHelper.max(new BigDecimal(6), new BigDecimal(12), new BigDecimal(11), new BigDecimal(-9), null, new BigDecimal(1));
        assertEquals(new BigDecimal(12), max);
    }

    @Test
    public void shouldGetTheRightMinValue() {

        BigDecimal min = NumberHelper.min(new BigDecimal(6), new BigDecimal(12), new BigDecimal(11), new BigDecimal(-9), null, new BigDecimal(1));
        assertEquals(new BigDecimal(-9), min);
    }
}