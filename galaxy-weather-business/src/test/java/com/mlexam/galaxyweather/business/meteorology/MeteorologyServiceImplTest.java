package com.mlexam.galaxyweather.business.meteorology;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.Closeable;
import com.mlexam.galaxyweather.business.simulation.GalaxyWeatherPrediction;
import com.mlexam.galaxyweather.business.simulation.GalaxyWeatherPredictionSummary;
import com.mlexam.galaxyweather.business.simulation.WeatherPredictor;
import com.mlexam.galaxyweather.business.space.PlanetDescriptor;
import com.mlexam.galaxyweather.business.space.PlanetManager;
import com.mlexam.galaxyweather.business.space.mechanics.MovablePlanet;
import com.mlexam.galaxyweather.dal.infrastructure.objectify.ObjectifyEntitiesRegistrar;
import com.mlexam.galaxyweather.dal.repository.Repository;
import com.mlexam.galaxyweather.dal.repository.weather.WeatherConditionRepository;
import com.mlexam.galaxyweather.model.entity.PlanetState;
import com.mlexam.galaxyweather.model.entity.SystemDescription;
import com.mlexam.galaxyweather.model.entity.WeatherCondition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MeteorologyServiceImplTest {

    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig().setDefaultHighRepJobPolicyUnappliedJobPercentage(100));
    private Closeable objectifyContext;

    @Mock
    private WeatherPredictor weatherPredictor;

    @Mock
    private WeatherConditionRepository weatherConditionRepository;

    @Mock
    private Repository<SystemDescription> systemDescriptionRepository;

    @Mock
    private Repository<PlanetState> planetStateRepository;

    @Mock
    private PlanetManager planetManager;

    @InjectMocks
    private MeteorologyService meteorologyService;

    @Before
    public void setup() throws Exception {
        helper.setUp();
        ObjectifyEntitiesRegistrar.register();
        objectifyContext = ObjectifyService.begin();
        meteorologyService = new MeteorologyServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {
        objectifyContext.close();
        helper.tearDown();
    }

    @Test
    public void shouldGetTheSummaryForTheNextTenYears() {

        // Hay una prediccion previamente procesada, con fecha de inicio del dia de hoy
        when(systemDescriptionRepository.getById(anyString())).thenReturn(MeteorologyServiceImplTestFixture.buildSystemDescriptor(0));

        // Existe dia registrado hasta 10 años desde la fecha inicial
        when(weatherConditionRepository.getLastDay()).thenReturn(MeteorologyServiceImplTestFixture.buildLastPredictedWeather(10 * 365));

        when(weatherConditionRepository.getBetweenDays(anyInt(), anyInt())).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                int fromDayNonInclusive = (int) invocation.getArguments()[0];
                int toDayInclusive = (int) invocation.getArguments()[1];
                return MeteorologyServiceImplTestFixture.buildDummyWeatherConditionList(toDayInclusive - fromDayNonInclusive);
            }
        });

        when(weatherPredictor.summarizePrediction(anyListOf(WeatherCondition.class), any(BigDecimal.class), anyInt()))
                .thenAnswer(new Answer<Object>() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        @SuppressWarnings("unchecked") List<WeatherCondition> weatherConditions = (List<WeatherCondition>) invocation.getArguments()[0];
                        return new GalaxyWeatherPredictionSummary(weatherConditions.size(), 0, 0, 0, new ArrayList<Integer>());
                    }
                });

        PredictionSummary prediction = meteorologyService.predictNextTenYears();

        assertEquals(10 * 365, prediction.getTotalPredictedDays());
    }

    @Test
    public void shouldGetThePredictionForADesiredDay() {

        // Hay una prediccion previamente procesada, con fecha de inicio de hace dos dias
        when(systemDescriptionRepository.getById(anyString())).thenReturn(MeteorologyServiceImplTestFixture.buildSystemDescriptor(-2));

        when(weatherConditionRepository.getByDay(anyInt())).thenReturn(MeteorologyServiceImplTestFixture.buildWeatherCondition(1, com.mlexam.galaxyweather.model.entity.WeatherKind.RAIN));
        when(weatherConditionRepository.getByDay(eq(20 + 2))).thenReturn(MeteorologyServiceImplTestFixture.buildWeatherCondition(20 + 2, com.mlexam.galaxyweather.model.entity.WeatherKind.DRY));

        WeatherKind prediction = meteorologyService.getSingleDayPrediction(20);

        assertEquals(WeatherKind.DRY, prediction);
    }

    @Test
    public void shouldProcessThePredictionForTheNextTenYearsIfNoPreviousRecordExists() {

        when(weatherPredictor.predictBehavior(any(MovablePlanet.class), any(MovablePlanet.class), any(MovablePlanet.class), anyInt(), any(BigDecimal.class)))
                .thenAnswer(new Answer<Object>() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        int receivedDays = (int) invocation.getArguments()[3];
                        return new GalaxyWeatherPrediction(MeteorologyServiceImplTestFixture.buildDummyPredictionDetailsList(receivedDays), 0, 0, 0, null, null);
                    }
                });

        when(systemDescriptionRepository.getById(anyString())).thenReturn(null);

        when(planetManager.getPlanet(any(PlanetDescriptor.class)))
                .thenReturn(MeteorologyServiceImplTestFixture.buildPlanet(PlanetDescriptor.BETASOIDE),
                        MeteorologyServiceImplTestFixture.buildPlanet(PlanetDescriptor.FERENGI),
                        MeteorologyServiceImplTestFixture.buildPlanet(PlanetDescriptor.VULCANO));

        meteorologyService.processNextTenYearsPrediction();

        verify(weatherPredictor).predictBehavior(any(MovablePlanet.class), any(MovablePlanet.class), any(MovablePlanet.class), eq(10 * 365), any(BigDecimal.class));
    }

    @Test
    public void shouldProcessThePredictionForTheNextTenYearsConsideringTheAlreadyPredictedValues() {

        when(weatherPredictor.predictBehavior(any(MovablePlanet.class), any(MovablePlanet.class), any(MovablePlanet.class), anyInt(), any(BigDecimal.class)))
                .thenAnswer(new Answer<Object>() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        int receivedDays = (int) invocation.getArguments()[3];
                        return new GalaxyWeatherPrediction(MeteorologyServiceImplTestFixture.buildDummyPredictionDetailsList(receivedDays), 0, 0, 0, null, null);
                    }
                });

        // Hay una prediccion previamente procesada, con fecha de inicio de hace 5 dias
        when(systemDescriptionRepository.getById(anyString())).thenReturn(MeteorologyServiceImplTestFixture.buildSystemDescriptor(-5));

        // Existe dia registrado hasta 10 años desde la fecha inicial
        when(weatherConditionRepository.getLastDay()).thenReturn(MeteorologyServiceImplTestFixture.buildLastPredictedWeather(10 * 365));

        when(planetManager.getPlanet(any(PlanetDescriptor.class)))
                .thenReturn(MeteorologyServiceImplTestFixture.buildPlanet(PlanetDescriptor.BETASOIDE),
                        MeteorologyServiceImplTestFixture.buildPlanet(PlanetDescriptor.FERENGI),
                        MeteorologyServiceImplTestFixture.buildPlanet(PlanetDescriptor.VULCANO));

        when(planetStateRepository.getById(anyString(), anyObject())).thenReturn(MeteorologyServiceImplTestFixture.buildPlanetState());

        meteorologyService.processNextTenYearsPrediction();

        verify(weatherPredictor).predictBehavior(any(MovablePlanet.class), any(MovablePlanet.class), any(MovablePlanet.class), eq(5), any(BigDecimal.class));
    }
}