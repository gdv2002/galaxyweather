package com.mlexam.galaxyweather.business.simulation;

import com.mlexam.galaxyweather.model.entity.WeatherCondition;
import com.mlexam.galaxyweather.model.entity.WeatherKind;

public class WeatherPredictorImplTestFixture {

    public static WeatherCondition buildWeatherCondition(int day, WeatherKind weatherKind, double perimeter) {
        WeatherCondition weatherCondition = new WeatherCondition();
        weatherCondition.setDay(day);
        weatherCondition.setWeatherKind(weatherKind);
        weatherCondition.setPerimeter(perimeter);
        return weatherCondition;
    }
}
