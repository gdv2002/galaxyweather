package com.mlexam.galaxyweather.business.meteorology;

import com.mlexam.galaxyweather.business.geometry.TriangleShapeInfo;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class WeatherAnalyzerImplTest {

    private WeatherAnalyzer weatherAnalyzer;

    @Before
    public void setup() {
        weatherAnalyzer = new WeatherAnalyzerImpl();
    }

    @Test
    public void systemShouldBeInDrought() {
        // Linea que contiene al origen
        TriangleShapeInfo infoActualDay = new TriangleShapeInfo(true, BigDecimal.TEN, BigDecimal.ONE);
        TriangleShapeInfo infoDayBefore = new TriangleShapeInfo(false, BigDecimal.TEN, BigDecimal.ONE.negate());
        GalaxyWeatherSummary summary = weatherAnalyzer.analyzeSystem(infoActualDay, infoDayBefore);
        assertEquals(WeatherKind.DRY, summary.getWeatherKind());

        // Linea que contiene al origen
        infoActualDay = new TriangleShapeInfo(true, BigDecimal.ZERO, BigDecimal.ZERO);
        infoDayBefore = new TriangleShapeInfo(false, BigDecimal.TEN, BigDecimal.ONE);
        summary = weatherAnalyzer.analyzeSystem(infoActualDay, infoDayBefore);
        assertEquals(WeatherKind.DRY, summary.getWeatherKind());
    }

    @Test
    public void systemShouldBeRainy() {
        // Formando un triangulo que contiene al origen
        TriangleShapeInfo infoActualDay = new TriangleShapeInfo(true, BigDecimal.TEN, BigDecimal.ONE);
        TriangleShapeInfo infoDayBefore = new TriangleShapeInfo(true, new BigDecimal(20), new BigDecimal(25));
        GalaxyWeatherSummary summary = weatherAnalyzer.analyzeSystem(infoActualDay, infoDayBefore);
        assertEquals(WeatherKind.RAIN, summary.getWeatherKind());
    }

    @Test
    public void systemWeatherShouldBeOptimal() {
        // Tres puntos sobre una misma linea, sin contener al origen
        TriangleShapeInfo infoActualDay = new TriangleShapeInfo(false, BigDecimal.ZERO, BigDecimal.ZERO);
        TriangleShapeInfo infoDayBefore = new TriangleShapeInfo(true, new BigDecimal(20), new BigDecimal(25));
        GalaxyWeatherSummary summary = weatherAnalyzer.analyzeSystem(infoActualDay, infoDayBefore);
        assertEquals(WeatherKind.OPTIMAL, summary.getWeatherKind());
    }

    @Test
    public void systemWeatherShouldBeUndetermined() {
        // Formando un triangulo que no contiene al origen
        TriangleShapeInfo infoActualDay = new TriangleShapeInfo(false, BigDecimal.TEN, BigDecimal.ONE);
        TriangleShapeInfo infoDayBefore = new TriangleShapeInfo(true, new BigDecimal(20), new BigDecimal(25));
        GalaxyWeatherSummary summary = weatherAnalyzer.analyzeSystem(infoActualDay, infoDayBefore);
        assertEquals(WeatherKind.UNDETERMINED, summary.getWeatherKind());
    }
}