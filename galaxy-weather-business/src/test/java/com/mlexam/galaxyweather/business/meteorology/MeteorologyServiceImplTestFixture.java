package com.mlexam.galaxyweather.business.meteorology;

import com.mlexam.galaxyweather.business.simulation.DayPredictionDetail;
import com.mlexam.galaxyweather.business.space.PlanetDescriptor;
import com.mlexam.galaxyweather.model.entity.Planet;
import com.mlexam.galaxyweather.model.entity.PlanetState;
import com.mlexam.galaxyweather.model.entity.SystemDescription;
import com.mlexam.galaxyweather.model.entity.WeatherCondition;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MeteorologyServiceImplTestFixture {

    public static List<DayPredictionDetail> buildDummyPredictionDetailsList(int days) {
        List<DayPredictionDetail> dummyList = new ArrayList<>(days);
        for (int i = 0; i < days; i++) {
            dummyList.add(new DayPredictionDetail(WeatherKind.UNDETERMINED, 1, 0, 1, 0, 1, 0, BigDecimal.ONE));
        }
        return dummyList;
    }

    public static PlanetState buildPlanetState() {
        PlanetState planetState = new PlanetState();
        planetState.setAngle(0);
        planetState.setDay(1);
        return planetState;
    }

    public static Planet buildPlanet(PlanetDescriptor planetDescriptor) {
        Planet planet = new Planet();
        planet.setId(planetDescriptor.getInternalId());
        return planet;
    }

    public static SystemDescription buildSystemDescriptor(int initialDayOffset) {
        SystemDescription systemDescription = new SystemDescription();
        systemDescription.setId(SystemDescription.UNIQUE_ID);
        systemDescription.setInitialDateInUtc(LocalDate.now(DateTimeZone.UTC).plusDays(initialDayOffset).toDate());
        systemDescription.setMaxKnownRainyDayPerimeter(0d);
        return systemDescription;
    }

    public static WeatherCondition buildLastPredictedWeather(int day) {
        WeatherCondition lastPredictedWeather = new WeatherCondition();
        lastPredictedWeather.setId(String.format("weatherday%d", day));
        lastPredictedWeather.setDay(day);
        lastPredictedWeather.setWeatherKind(com.mlexam.galaxyweather.model.entity.WeatherKind.UNDETERMINED);
        return lastPredictedWeather;
    }

    public static List<WeatherCondition> buildDummyWeatherConditionList(int days) {
        List<WeatherCondition> dummyList = new ArrayList<>(days);
        for (int i = 0; i < days; i++) {
            WeatherCondition weatherCondition = new WeatherCondition();
            weatherCondition.setDay(i + 1);
            weatherCondition.setPerimeter(Math.random());
            weatherCondition.setWeatherKind(com.mlexam.galaxyweather.model.entity.WeatherKind.UNDETERMINED);
            dummyList.add(weatherCondition);
        }
        return dummyList;
    }

    public static WeatherCondition buildWeatherCondition(int day, com.mlexam.galaxyweather.model.entity.WeatherKind weatherKind) {
        WeatherCondition weatherCondition = new WeatherCondition();
        weatherCondition.setDay(day);
        weatherCondition.setPerimeter(Math.random());
        weatherCondition.setWeatherKind(weatherKind);
        return weatherCondition;
    }
}
