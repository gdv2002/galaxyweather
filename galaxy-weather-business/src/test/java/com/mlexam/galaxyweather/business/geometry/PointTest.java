package com.mlexam.galaxyweather.business.geometry;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.*;

public class PointTest {

    @Test
    public void shouldCalculateTheDistanceBetweenTwoPoints() {
        Point point1 = new Point(10, 5);
        Point point2 = new Point(100, 50);
        BigDecimal distance = point1.distance(point2).setScale(4, RoundingMode.HALF_EVEN);
        assertEquals(new BigDecimal(100.6231).setScale(4, RoundingMode.HALF_EVEN), distance);

        point1 = new Point(-20, 5);
        point2 = new Point(40, 30);
        distance = point1.distance(point2).setScale(4, RoundingMode.HALF_EVEN);
        assertEquals(new BigDecimal(65).setScale(4, RoundingMode.HALF_EVEN), distance);

        point1 = new Point(30, 40);
        point2 = new Point(-20, 80);
        distance = point1.distance(point2).setScale(4, RoundingMode.HALF_EVEN);
        assertEquals(new BigDecimal(64.0312).setScale(4, RoundingMode.HALF_EVEN), distance);
    }
}