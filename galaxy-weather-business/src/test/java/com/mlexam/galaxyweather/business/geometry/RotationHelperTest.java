package com.mlexam.galaxyweather.business.geometry;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RotationHelperTest {

    private RotationHelper rotationHelper;

    @Before
    public void setup() {
        rotationHelper = new RotationHelper();
    }

    @Test
    public void shouldAddDegreesCorrectly() {
        double initial = 10d;
        double end = rotationHelper.addDegrees(initial, 20d);

        assertEquals(30d, end, 0d);
    }

    @Test
    public void shouldSubtractDegreesCorrectly() {
        double initial = 10d;
        double end = rotationHelper.addDegrees(initial, -40d);

        assertEquals(330d, end, 0d);
    }

    @Test
    public void shouldAddDegreesCorrectlyWhenGoesOver360() {
        double initial = 320d;
        double end = rotationHelper.addDegrees(initial, 50d);
        assertEquals(10d, end, 0d);

        end = rotationHelper.addDegrees(initial, 500d);
        assertEquals(100d, end, 0d);
    }

    @Test
    public void shouldSubtractDegreesCorrectlyWhenGoesUnderZero() {
        double initial = 5d;
        double end = rotationHelper.addDegrees(initial, -20d);
        assertEquals(345d, end, 0d);

        end = rotationHelper.addDegrees(initial, -850d);
        assertEquals(235d, end, 0d);
    }
}