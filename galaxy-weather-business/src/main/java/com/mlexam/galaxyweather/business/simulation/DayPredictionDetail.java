package com.mlexam.galaxyweather.business.simulation;

import com.mlexam.galaxyweather.business.meteorology.WeatherKind;

import java.math.BigDecimal;

public class DayPredictionDetail {

    private final WeatherKind weatherKind;

    private double planet1radius;
    private double planet1AngleInDegrees;
    private double planet2radius;
    private double planet2AngleInDegrees;
    private double planet3radius;
    private double planet3AngleInDegrees;

    private BigDecimal perimeterBetweenPlanets;

    public DayPredictionDetail(WeatherKind weatherKind, double planet1radius, double planet1AngleInDegrees, double planet2radius, double planet2AngleInDegrees, double planet3radius, double planet3AngleInDegrees, BigDecimal perimeterBetweenPlanets) {
        this.weatherKind = weatherKind;
        this.planet1radius = planet1radius;
        this.planet1AngleInDegrees = planet1AngleInDegrees;
        this.planet2radius = planet2radius;
        this.planet2AngleInDegrees = planet2AngleInDegrees;
        this.planet3radius = planet3radius;
        this.planet3AngleInDegrees = planet3AngleInDegrees;
        this.perimeterBetweenPlanets = perimeterBetweenPlanets;
    }

    public WeatherKind getWeatherKind() {
        return weatherKind;
    }

    public double getPlanet1radius() {
        return planet1radius;
    }

    public double getPlanet1AngleInDegrees() {
        return planet1AngleInDegrees;
    }

    public double getPlanet2radius() {
        return planet2radius;
    }

    public double getPlanet2AngleInDegrees() {
        return planet2AngleInDegrees;
    }

    public double getPlanet3radius() {
        return planet3radius;
    }

    public double getPlanet3AngleInDegrees() {
        return planet3AngleInDegrees;
    }

    public BigDecimal getPerimeterBetweenPlanets() {
        return perimeterBetweenPlanets;
    }
}
