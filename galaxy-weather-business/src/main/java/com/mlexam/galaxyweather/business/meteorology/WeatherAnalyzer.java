package com.mlexam.galaxyweather.business.meteorology;

import com.mlexam.galaxyweather.business.geometry.TriangleShapeInfo;

public interface WeatherAnalyzer {

    GalaxyWeatherSummary analyzeSystem(TriangleShapeInfo infoActualDay, TriangleShapeInfo infoDayBefore);
}
