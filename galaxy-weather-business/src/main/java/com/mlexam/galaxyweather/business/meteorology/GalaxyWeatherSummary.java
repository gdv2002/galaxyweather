package com.mlexam.galaxyweather.business.meteorology;

import java.math.BigDecimal;

public class GalaxyWeatherSummary {

    private final WeatherKind weatherKind;
    private final BigDecimal perimeter;

    public GalaxyWeatherSummary(WeatherKind weatherKind, BigDecimal perimeter) {
        this.weatherKind = weatherKind;
        this.perimeter = perimeter;
    }

    public WeatherKind getWeatherKind() {
        return weatherKind;
    }

    public BigDecimal getPerimeter() {
        return perimeter;
    }
}
