package com.mlexam.galaxyweather.business.meteorology;

import com.mlexam.galaxyweather.business.geometry.TriangleShapeInfo;

import java.math.BigDecimal;

public class WeatherAnalyzerImpl implements WeatherAnalyzer {

    @Override
    public GalaxyWeatherSummary analyzeSystem(TriangleShapeInfo infoActualDay, TriangleShapeInfo infoDayBefore) {

        if (infoActualDay.getArea().equals(BigDecimal.ZERO))
            return resolveAlignedSystem(infoActualDay);

        if (infoDayBefore.getArea().equals(BigDecimal.ZERO))
            return resolveNonAlignedSystem(infoActualDay);

        if (infoActualDay.getArea().signum() != infoDayBefore.getArea().signum())
            // Si hubo cambio de signo en las areas, entonces los puntos formaron una linea recta en algún momento entre el día anterior y el actual
            return resolveAlignedSystem(infoActualDay);

        return resolveNonAlignedSystem(infoActualDay);
    }

    private GalaxyWeatherSummary resolveAlignedSystem(TriangleShapeInfo info) {
        if (info.isOriginContained())
            return new GalaxyWeatherSummary(WeatherKind.DRY, info.getPerimeter());
        return new GalaxyWeatherSummary(WeatherKind.OPTIMAL, info.getPerimeter());
    }

    private GalaxyWeatherSummary resolveNonAlignedSystem(TriangleShapeInfo info) {
        if (info.isOriginContained())
            return new GalaxyWeatherSummary(WeatherKind.RAIN, info.getPerimeter());
        return new GalaxyWeatherSummary(WeatherKind.UNDETERMINED, info.getPerimeter());
    }
}
