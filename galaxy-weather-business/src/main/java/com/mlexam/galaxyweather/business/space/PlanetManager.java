package com.mlexam.galaxyweather.business.space;

import com.mlexam.galaxyweather.model.entity.Planet;

public interface PlanetManager {

    void initialize();

    Planet getPlanet(PlanetDescriptor planetDescriptor);
}
