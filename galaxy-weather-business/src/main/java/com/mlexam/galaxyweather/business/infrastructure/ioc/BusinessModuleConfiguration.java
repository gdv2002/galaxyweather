package com.mlexam.galaxyweather.business.infrastructure.ioc;

import com.mlexam.galaxyweather.business.meteorology.MeteorologyService;
import com.mlexam.galaxyweather.business.meteorology.MeteorologyServiceImpl;
import com.mlexam.galaxyweather.business.meteorology.WeatherAnalyzer;
import com.mlexam.galaxyweather.business.meteorology.WeatherAnalyzerImpl;
import com.mlexam.galaxyweather.business.simulation.WeatherPredictor;
import com.mlexam.galaxyweather.business.simulation.WeatherPredictorImpl;
import com.mlexam.galaxyweather.business.space.PlanetManager;
import com.mlexam.galaxyweather.business.space.PlanetManagerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SuppressWarnings("unused")
@Configuration
public class BusinessModuleConfiguration {

    @Bean
    public WeatherAnalyzer weatherAnalyzer() {
        return new WeatherAnalyzerImpl();
    }

    @Bean
    public WeatherPredictor weatherPrediction() {
        return new WeatherPredictorImpl();
    }

    @Bean
    public MeteorologyService meteorologyService() {
        return new MeteorologyServiceImpl();
    }

    @Bean
    public PlanetManager planetManager() {
        return new PlanetManagerImpl();
    }

    @Bean
    public SystemInitialization initialization() {
        return new SystemInitialization();
    }
}
