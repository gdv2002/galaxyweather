package com.mlexam.galaxyweather.business.meteorology;

public interface MeteorologyService {

    PredictionSummary predictNextTenYears();

    WeatherKind getSingleDayPrediction(int day);

    void processNextTenYearsPrediction();
}
