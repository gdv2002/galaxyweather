package com.mlexam.galaxyweather.business.simulation;

import com.mlexam.galaxyweather.business.space.mechanics.MovablePlanet;
import com.mlexam.galaxyweather.model.entity.WeatherCondition;

import java.math.BigDecimal;
import java.util.List;

public interface WeatherPredictor {

    GalaxyWeatherPrediction predictBehavior(MovablePlanet movablePlanet1, MovablePlanet movablePlanet2, MovablePlanet movablePlanet3, int days, BigDecimal knownMaxRainyDayPerimeter);

    GalaxyWeatherPredictionSummary summarizePrediction(List<WeatherCondition> weatherConditions, BigDecimal knownMaxRainyDayPerimeter, int daysElapsedSinceTheInitialDay);
}
