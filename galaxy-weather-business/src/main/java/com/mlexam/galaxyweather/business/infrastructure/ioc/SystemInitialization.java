package com.mlexam.galaxyweather.business.infrastructure.ioc;

import com.mlexam.galaxyweather.business.space.PlanetManager;
import org.springframework.beans.factory.annotation.Autowired;

public class SystemInitialization {

    @Autowired
    private PlanetManager planetManager;

    public void setUp() {
        planetManager.initialize();
    }
}
