package com.mlexam.galaxyweather.business.geometry;

public class RotationHelper {

    public double addDegrees(double initialDegrees, double toAdd) {

        if (initialDegrees < 0d || initialDegrees > 360d)
            throw new IllegalArgumentException("initialDegrees must be between 0 and 360");
        if (initialDegrees == 360d)
            initialDegrees = 0d;

        double result = initialDegrees + toAdd;
        while (result >= 360d)
            result -= 360d;
        while (result < 0)
            result += 360d;

        return result;
    }
}
