package com.mlexam.galaxyweather.business.geometry;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Point {

    private final double x;
    private final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public BigDecimal distance(Point point) {
        double diffX = point.getX() - getX();
        double diffY = point.getY() - getY();

        return new BigDecimal(Math.sqrt((diffX * diffX) + (diffY * diffY))).setScale(4, RoundingMode.HALF_EVEN);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Point) {
            Point p = (Point) obj;
            return (getX() == p.getX()) && (getY() == p.getY());
        }
        return super.equals(obj);
    }
}
