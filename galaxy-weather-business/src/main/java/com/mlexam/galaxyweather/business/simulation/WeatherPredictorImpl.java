package com.mlexam.galaxyweather.business.simulation;

import com.mlexam.galaxyweather.business.geometry.TriangleShapeInfo;
import com.mlexam.galaxyweather.business.geometry.TrigonometryHelper;
import com.mlexam.galaxyweather.business.math.NumberHelper;
import com.mlexam.galaxyweather.business.meteorology.GalaxyWeatherSummary;
import com.mlexam.galaxyweather.business.meteorology.WeatherAnalyzer;
import com.mlexam.galaxyweather.business.space.mechanics.MovablePlanet;
import com.mlexam.galaxyweather.model.entity.WeatherCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class WeatherPredictorImpl implements WeatherPredictor {

    @SuppressWarnings("unused")
    private final Logger logger = LoggerFactory.getLogger(WeatherPredictorImpl.class);

    @Autowired
    private WeatherAnalyzer weatherAnalyzer;

    private TrigonometryHelper trigonometryHelper = new TrigonometryHelper();

    @Override
    public GalaxyWeatherPrediction predictBehavior(MovablePlanet movablePlanet1, MovablePlanet movablePlanet2, MovablePlanet movablePlanet3, int days, BigDecimal knownMaxRainyDayPerimeter) {

        if (days < 1)
            throw new IllegalArgumentException("days must be greather than or equal to one");

        List<DayPredictionDetail> predictionDetails = new ArrayList<>(days);
        int dryDaysCount = 0;
        int rainyDaysCount = 0;
        int optimalWeatherDaysCount = 0;

        BigDecimal maxRainyDayPerimeter = knownMaxRainyDayPerimeter != null ? knownMaxRainyDayPerimeter : BigDecimal.ZERO;
        List<Integer> topRainyDays = new ArrayList<>();

        TriangleShapeInfo infoDayBefore = trigonometryHelper.analyzeTriangle(movablePlanet1.getCurrentLocation(), movablePlanet2.getCurrentLocation(), movablePlanet3.getCurrentLocation());

        for (int i = 0; i < days; i++) {
            movablePlanet1.move(1);
            movablePlanet2.move(1);
            movablePlanet3.move(1);

            TriangleShapeInfo infoActualDay = trigonometryHelper.analyzeTriangle(movablePlanet1.getCurrentLocation(), movablePlanet2.getCurrentLocation(), movablePlanet3.getCurrentLocation());
            GalaxyWeatherSummary summary = weatherAnalyzer.analyzeSystem(infoActualDay, infoDayBefore);

//            logger.info("Dia: {} - Area: {}", i + 1, infoActualDay.getArea());

            infoDayBefore = infoActualDay;

            predictionDetails.add(new DayPredictionDetail(summary.getWeatherKind(),
                    movablePlanet1.getRadius(), movablePlanet1.getCurrentAngleInDegrees(),
                    movablePlanet2.getRadius(), movablePlanet2.getCurrentAngleInDegrees(),
                    movablePlanet3.getRadius(), movablePlanet3.getCurrentAngleInDegrees(),
                    summary.getPerimeter()));

            switch (summary.getWeatherKind()) {
                case RAIN:
                    rainyDaysCount++;
                    if (summary.getPerimeter().compareTo(maxRainyDayPerimeter) > 0) {
                        maxRainyDayPerimeter = summary.getPerimeter();
                        topRainyDays = new ArrayList<>();
                        topRainyDays.add(i + 1);
                    } else if (NumberHelper.areEqual(summary.getPerimeter(), maxRainyDayPerimeter, new BigDecimal(0.0001d))) {
                        topRainyDays.add(i + 1);
                    }
                    break;
                case OPTIMAL:
                    optimalWeatherDaysCount++;
                    break;
                case DRY:
                    dryDaysCount++;
                    break;
            }
        }

        return new GalaxyWeatherPrediction(predictionDetails, dryDaysCount, rainyDaysCount, optimalWeatherDaysCount, topRainyDays, maxRainyDayPerimeter);
    }

    @Override
    public GalaxyWeatherPredictionSummary summarizePrediction(List<WeatherCondition> weatherConditions, BigDecimal knownMaxRainyDayPerimeter, int daysElapsedSinceTheInitialDay) {

        int dryDaysCount = 0;
        int rainyDaysCount = 0;
        int optimalWeatherDaysCount = 0;

        List<Integer> topRainyDays = new ArrayList<>();

        for (WeatherCondition weatherCondition : weatherConditions) {

            switch (weatherCondition.getWeatherKind()) {
                case RAIN:
                    rainyDaysCount++;
                    if (NumberHelper.areEqual(new BigDecimal(weatherCondition.getPerimeter()), knownMaxRainyDayPerimeter, new BigDecimal(0.0001d)))
                        topRainyDays.add(weatherCondition.getDay() - daysElapsedSinceTheInitialDay);
                    break;
                case OPTIMAL:
                    optimalWeatherDaysCount++;
                    break;
                case DRY:
                    dryDaysCount++;
                    break;
            }
        }

        return new GalaxyWeatherPredictionSummary(weatherConditions.size(), dryDaysCount, rainyDaysCount, optimalWeatherDaysCount, topRainyDays);
    }

    @SuppressWarnings("unused")
    public void setWeatherAnalyzer(WeatherAnalyzer weatherAnalyzer) {
        this.weatherAnalyzer = weatherAnalyzer;
    }
}
