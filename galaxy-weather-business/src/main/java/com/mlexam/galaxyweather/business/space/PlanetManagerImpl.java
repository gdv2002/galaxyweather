package com.mlexam.galaxyweather.business.space;

import com.googlecode.objectify.VoidWork;
import com.mlexam.galaxyweather.business.space.mechanics.DistantGalaxy;
import com.mlexam.galaxyweather.business.space.mechanics.MovablePlanet;
import com.mlexam.galaxyweather.dal.repository.Repository;
import com.mlexam.galaxyweather.model.entity.Planet;
import org.springframework.beans.factory.annotation.Autowired;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class PlanetManagerImpl implements PlanetManager {

    @Autowired
    private Repository<Planet> planetRepository;

    @Override
    public void initialize() {

        final DistantGalaxy galaxy = new DistantGalaxy(DistantGalaxy.BETASOIDE_INITIAL_DEGREES, DistantGalaxy.FERENGI_INITIAL_DEGREES, DistantGalaxy.VULCANO_INITIAL_DEGREES);

        // Creo los planetas si no existen. Lo mas probable es que existan, asi que los busco fuera de la transaccion
        if (planetRepository.getById(PlanetDescriptor.BETASOIDE.getInternalId()) != null
                && planetRepository.getById(PlanetDescriptor.FERENGI.getInternalId()) != null
                && planetRepository.getById(PlanetDescriptor.VULCANO.getInternalId()) != null)
                return;

        ofy().transactNew(new VoidWork() {
            @Override
            public void vrun() {
                Planet betasoidePlanet = planetRepository.getById(PlanetDescriptor.BETASOIDE.getInternalId());
                if (betasoidePlanet == null) {
                    betasoidePlanet = buildPlanet(PlanetDescriptor.BETASOIDE, galaxy.getBetasoidePlanet(), "Betasoide");
                    planetRepository.save(betasoidePlanet);
                }

                Planet ferengiPlanet = planetRepository.getById(PlanetDescriptor.FERENGI.getInternalId());
                if (ferengiPlanet == null) {
                    ferengiPlanet = buildPlanet(PlanetDescriptor.FERENGI, galaxy.getFerengiPlanet(), "Ferengi");
                    planetRepository.save(ferengiPlanet);
                }

                Planet vulcanoPlanet = planetRepository.getById(PlanetDescriptor.VULCANO.getInternalId());
                if (vulcanoPlanet == null) {
                    vulcanoPlanet = buildPlanet(PlanetDescriptor.VULCANO, galaxy.getVulcanoPlanet(), "Vulcano");
                    planetRepository.save(vulcanoPlanet);
                }
            }
        });
    }

    private Planet buildPlanet(PlanetDescriptor planetDescriptor, MovablePlanet movablePlanet, String friendlyName) {
        Planet planet = new Planet();
        planet.setId(planetDescriptor.getInternalId());
        planet.setName(friendlyName);
        planet.setDistanceFromSun(movablePlanet.getRadius());
        planet.setRotationSpeed(movablePlanet.getAngularSpeedInDegrees());
        return planet;
    }

    @Override
    public Planet getPlanet(PlanetDescriptor planetDescriptor) {
        return planetRepository.getById(planetDescriptor.getInternalId());
    }

    @SuppressWarnings("unused")
    public void setPlanetRepository(Repository<Planet> planetRepository) {
        this.planetRepository = planetRepository;
    }
}
