package com.mlexam.galaxyweather.business.space;

public enum PlanetDescriptor {

    BETASOIDE("betasoide"), FERENGI("ferengi"), VULCANO("vulcano");

    private final String internalId;

    PlanetDescriptor(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalId() {
        return internalId;
    }
}
