package com.mlexam.galaxyweather.business.space.mechanics;

import com.mlexam.galaxyweather.business.geometry.Point;
import com.mlexam.galaxyweather.business.geometry.RotationHelper;
import com.mlexam.galaxyweather.business.geometry.TrigonometryHelper;

public class MovablePlanet {

    private RotationHelper rotationHelper = new RotationHelper();
    private TrigonometryHelper trigonometryHelper = new TrigonometryHelper();

    private final double angularSpeedInDegrees;
    private final double radius;
    private Point currentLocation;
    private double currentAngleInDegrees;

    public MovablePlanet(double radius, double angleInDegrees, double angularSpeedInDegrees) {
        this.radius = radius;
        this.currentAngleInDegrees = angleInDegrees;
        this.angularSpeedInDegrees = angularSpeedInDegrees;
        fillCurrentLocation();
    }

    public void move(int days) {
        currentAngleInDegrees = rotationHelper.addDegrees(currentAngleInDegrees, angularSpeedInDegrees * days);
        fillCurrentLocation();
    }

    private void fillCurrentLocation() {
        this.currentLocation = trigonometryHelper.getPoint(radius, currentAngleInDegrees);
    }

    public Point getCurrentLocation() {
        return currentLocation;
    }

    public double getRadius() {
        return radius;
    }

    public double getCurrentAngleInDegrees() {
        return currentAngleInDegrees;
    }

    public double getAngularSpeedInDegrees() {
        return angularSpeedInDegrees;
    }
}
