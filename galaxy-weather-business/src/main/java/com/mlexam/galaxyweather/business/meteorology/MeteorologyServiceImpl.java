package com.mlexam.galaxyweather.business.meteorology;

import com.googlecode.objectify.VoidWork;
import com.mlexam.galaxyweather.business.math.NumberHelper;
import com.mlexam.galaxyweather.business.simulation.DayPredictionDetail;
import com.mlexam.galaxyweather.business.simulation.GalaxyWeatherPrediction;
import com.mlexam.galaxyweather.business.simulation.GalaxyWeatherPredictionSummary;
import com.mlexam.galaxyweather.business.simulation.WeatherPredictor;
import com.mlexam.galaxyweather.business.space.PlanetDescriptor;
import com.mlexam.galaxyweather.business.space.PlanetManager;
import com.mlexam.galaxyweather.business.space.mechanics.DistantGalaxy;
import com.mlexam.galaxyweather.dal.repository.Repository;
import com.mlexam.galaxyweather.dal.repository.weather.WeatherConditionRepository;
import com.mlexam.galaxyweather.model.entity.Planet;
import com.mlexam.galaxyweather.model.entity.PlanetState;
import com.mlexam.galaxyweather.model.entity.SystemDescription;
import com.mlexam.galaxyweather.model.entity.WeatherCondition;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class MeteorologyServiceImpl implements MeteorologyService {

    public static final int TEN_YEARS = 10 * 365;

    @SuppressWarnings("unused")
    private final Logger logger = LoggerFactory.getLogger(MeteorologyServiceImpl.class);

    @Autowired
    private WeatherPredictor weatherPredictor;

    @Autowired
    private Repository<SystemDescription> systemDescriptionRepository;

    @Autowired
    private Repository<PlanetState> planetStateRepository;

    @Autowired
    private WeatherConditionRepository weatherConditionRepository;

    @Autowired
    private PlanetManager planetManager;

    @Override
    public PredictionSummary predictNextTenYears() {

        LocalDate today = LocalDate.now(DateTimeZone.UTC);

        SystemDescription systemDescription = systemDescriptionRepository.getById(SystemDescription.UNIQUE_ID);

        int daysElapsedSinceTheInitialDay = getDaysElapsedSinceTheInitialDay(systemDescription, today);
        int lastDayThatShouldBePredicted = getLastDayThatShouldBePredicted(daysElapsedSinceTheInitialDay);
        WeatherCondition lastPredictedDay = weatherConditionRepository.getLastDay();
        if (lastPredictedDay == null || lastPredictedDay.getDay() < lastDayThatShouldBePredicted) {
            // Faltan predicciones, probablemente porque no corrio el job que las genera
            processNextTenYearsPrediction();

            // Lo vuelvo a obtener por si era nulo o si cambió en el proceso
            systemDescription = systemDescriptionRepository.getById(SystemDescription.UNIQUE_ID);
        }

        List<WeatherCondition> weatherConditions = weatherConditionRepository.getBetweenDays(lastDayThatShouldBePredicted - TEN_YEARS, lastDayThatShouldBePredicted);

        GalaxyWeatherPredictionSummary summary = weatherPredictor.summarizePrediction(weatherConditions, new BigDecimal(systemDescription.getMaxKnownRainyDayPerimeter()), daysElapsedSinceTheInitialDay);

        return new PredictionSummary(summary.getTotalPredictedDays(), summary.getDryDaysCount(), summary.getRainyDaysCount(), summary.getOptimalWeatherDaysCount(), summary.getTopRainyDays());
    }

    @Override
    public WeatherKind getSingleDayPrediction(int day) {
        if (day < 1 || day > TEN_YEARS)
            throw new IllegalArgumentException(String.format("Requested day must be between 1 and %d", TEN_YEARS));

        LocalDate today = LocalDate.now(DateTimeZone.UTC);

        SystemDescription systemDescription = systemDescriptionRepository.getById(SystemDescription.UNIQUE_ID);

        int daysElapsedSinceTheInitialDay = getDaysElapsedSinceTheInitialDay(systemDescription, today);
        int actualRequestedDay = day + daysElapsedSinceTheInitialDay;

        WeatherCondition predictedDay = weatherConditionRepository.getByDay(actualRequestedDay);
        if (predictedDay == null) {
            // Faltan predicciones, probablemente porque no corrio el job que las genera
            processNextTenYearsPrediction();

            predictedDay = weatherConditionRepository.getByDay(actualRequestedDay);
        }

        return transformWeatherKind(predictedDay.getWeatherKind());
    }


    @Override
    public void processNextTenYearsPrediction() {

        LocalDate today = LocalDate.now(DateTimeZone.UTC);

        SystemDescription systemDescription = systemDescriptionRepository.getById(SystemDescription.UNIQUE_ID);

        int lastDayThatShouldBePredicted = getLastDayThatShouldBePredicted(systemDescription, today);
        WeatherCondition lastPredictedDay = weatherConditionRepository.getLastDay();

        int daysToPredict = lastDayThatShouldBePredicted;
        if (lastPredictedDay != null)
            daysToPredict -= lastPredictedDay.getDay();

        if (daysToPredict <= 0)
            return;


        Planet planetBetasoide = planetManager.getPlanet(PlanetDescriptor.BETASOIDE);
        Planet planetFerengi = planetManager.getPlanet(PlanetDescriptor.FERENGI);
        Planet planetVulcano = planetManager.getPlanet(PlanetDescriptor.VULCANO);

        double planetBetasoideCurrentAngle = DistantGalaxy.BETASOIDE_INITIAL_DEGREES;
        double planetFerengiCurrentAngle = DistantGalaxy.FERENGI_INITIAL_DEGREES;
        double planetVulcanoCurrentAngle = DistantGalaxy.VULCANO_INITIAL_DEGREES;

        if (lastPredictedDay != null) {
            PlanetState planetState = planetStateRepository.getById(buildPlanetStateId(PlanetDescriptor.BETASOIDE, lastPredictedDay.getDay()), planetBetasoide);
            planetBetasoideCurrentAngle = planetState.getAngle();
            planetState = planetStateRepository.getById(buildPlanetStateId(PlanetDescriptor.FERENGI, lastPredictedDay.getDay()), planetFerengi);
            planetFerengiCurrentAngle = planetState.getAngle();
            planetState = planetStateRepository.getById(buildPlanetStateId(PlanetDescriptor.VULCANO, lastPredictedDay.getDay()), planetVulcano);
            planetVulcanoCurrentAngle = planetState.getAngle();
        }

        DistantGalaxy galaxy = new DistantGalaxy(planetBetasoideCurrentAngle, planetFerengiCurrentAngle, planetVulcanoCurrentAngle);


        processPredictions(systemDescription, today, daysToPredict, lastPredictedDay, planetBetasoide, planetFerengi, planetVulcano, galaxy);
    }

    private int getDaysElapsedSinceTheInitialDay(SystemDescription systemDescription, LocalDate today) {
        int daysElapsedSinceTheInitialDay = 0;
        if (systemDescription != null) {
            LocalDate initialDay = LocalDate.fromDateFields(systemDescription.getInitialDateInUtc());
            daysElapsedSinceTheInitialDay = Days.daysBetween(initialDay, today).getDays();
        }
        return daysElapsedSinceTheInitialDay;
    }

    private int getLastDayThatShouldBePredicted(SystemDescription systemDescription, LocalDate today) {
        int daysElapsedSinceTheInitialDay = getDaysElapsedSinceTheInitialDay(systemDescription, today);
        return getLastDayThatShouldBePredicted(daysElapsedSinceTheInitialDay);
    }

    private int getLastDayThatShouldBePredicted(int daysElapsedSinceTheInitialDay) {
        return TEN_YEARS + daysElapsedSinceTheInitialDay;
    }

    private void processPredictions(final SystemDescription systemDescription, final LocalDate today, final int daysToPredict, final WeatherCondition lastPredictedDay,
                                    final Planet planetBetasoide, final Planet planetFerengi, final Planet planetVulcano, final DistantGalaxy galaxy) {

        ofy().transactNew(new VoidWork() {
            @Override
            public void vrun() {
                innerProcessPredictions(systemDescription, today, daysToPredict, lastPredictedDay, planetBetasoide, planetFerengi, planetVulcano, galaxy);
            }
        });
    }

    private void innerProcessPredictions(SystemDescription systemDescription, LocalDate today, int daysToPredict, WeatherCondition lastPredictedDay,
                                         Planet planetBetasoide, Planet planetFerengi, Planet planetVulcano, DistantGalaxy galaxy) {

        BigDecimal knownMaxRainyDayPerimeter = null;
        if (systemDescription != null) {
            systemDescription = systemDescriptionRepository.getById(systemDescription.getId());  // Me aseguro de tener una entity asociada a la transaccion actual
            knownMaxRainyDayPerimeter = new BigDecimal(systemDescription.getMaxKnownRainyDayPerimeter()).setScale(4, RoundingMode.HALF_EVEN);
        } else {
            systemDescription = new SystemDescription();
            systemDescription.setInitialDateInUtc(today.toDate());
            systemDescriptionRepository.save(systemDescription);
        }


        GalaxyWeatherPrediction galaxyWeatherPrediction = weatherPredictor.predictBehavior(galaxy.getBetasoidePlanet(), galaxy.getFerengiPlanet(), galaxy.getVulcanoPlanet(), daysToPredict, knownMaxRainyDayPerimeter);

        int day = lastPredictedDay != null ? lastPredictedDay.getDay() : 0;

        for (DayPredictionDetail dayPredictionDetail : galaxyWeatherPrediction.getDayPredictionDetails()) {
            day++;

            PlanetState planetState = new PlanetState();
            planetState.setId(String.format("%sday%d", PlanetDescriptor.BETASOIDE.getInternalId(), day));
            planetState.setDay(day);
            planetState.setAngle(dayPredictionDetail.getPlanet1AngleInDegrees());
            planetState.setPlanet(planetBetasoide);
            planetStateRepository.save(planetState);

            planetState = new PlanetState();
            planetState.setId(String.format("%sday%d", PlanetDescriptor.FERENGI.getInternalId(), day));
            planetState.setDay(day);
            planetState.setAngle(dayPredictionDetail.getPlanet2AngleInDegrees());
            planetState.setPlanet(planetFerengi);
            planetStateRepository.save(planetState);

            planetState = new PlanetState();
            planetState.setId(String.format("%sday%d", PlanetDescriptor.VULCANO.getInternalId(), day));
            planetState.setDay(day);
            planetState.setAngle(dayPredictionDetail.getPlanet3AngleInDegrees());
            planetState.setPlanet(planetVulcano);
            planetStateRepository.save(planetState);

            WeatherCondition weatherCondition = new WeatherCondition();
            weatherCondition.setId(String.format("conditionday%d", day));
            weatherCondition.setDay(day);
            weatherCondition.setWeatherKind(transformWeatherKind(dayPredictionDetail.getWeatherKind()));
            weatherCondition.setPerimeter(dayPredictionDetail.getPerimeterBetweenPlanets().doubleValue());
            weatherConditionRepository.save(weatherCondition);
        }

        if ((knownMaxRainyDayPerimeter == null)
                || (galaxyWeatherPrediction.getMaxRainyDayPerimeter() != null && galaxyWeatherPrediction.getMaxRainyDayPerimeter().compareTo(knownMaxRainyDayPerimeter) > 0)) {
            systemDescription.setMaxKnownRainyDayPerimeter(NumberHelper.safelyExtractDouble(galaxyWeatherPrediction.getMaxRainyDayPerimeter(), 0d));
            systemDescriptionRepository.save(systemDescription);
        }

        logger.info("Last predicted day: {}", day);
    }

    private String buildPlanetStateId(PlanetDescriptor planetDescriptor, int day) {
        return String.format("%sday%d", planetDescriptor.getInternalId(), day);
    }

    private com.mlexam.galaxyweather.model.entity.WeatherKind transformWeatherKind(WeatherKind weatherKind) {
        switch (weatherKind) {
            case RAIN:
                return com.mlexam.galaxyweather.model.entity.WeatherKind.RAIN;
            case OPTIMAL:
                return com.mlexam.galaxyweather.model.entity.WeatherKind.OPTIMAL;
            case UNDETERMINED:
                return com.mlexam.galaxyweather.model.entity.WeatherKind.UNDETERMINED;
            case DRY:
                return com.mlexam.galaxyweather.model.entity.WeatherKind.DRY;
        }
        throw new IllegalArgumentException(String.format("unrecognized weather kind %s", weatherKind.toString()));
    }

    private WeatherKind transformWeatherKind(com.mlexam.galaxyweather.model.entity.WeatherKind weatherKind) {
        switch (weatherKind) {
            case RAIN:
                return WeatherKind.RAIN;
            case OPTIMAL:
                return WeatherKind.OPTIMAL;
            case UNDETERMINED:
                return WeatherKind.UNDETERMINED;
            case DRY:
                return WeatherKind.DRY;
        }
        throw new IllegalArgumentException(String.format("unrecognized weather kind %s", weatherKind.toString()));
    }

    @SuppressWarnings("unused")
    public void setWeatherPredictor(WeatherPredictor weatherPredictor) {
        this.weatherPredictor = weatherPredictor;
    }

    @SuppressWarnings("unused")
    public void setSystemDescriptionRepository(Repository<SystemDescription> systemDescriptionRepository) {
        this.systemDescriptionRepository = systemDescriptionRepository;
    }

    @SuppressWarnings("unused")
    public void setPlanetManager(PlanetManager planetManager) {
        this.planetManager = planetManager;
    }

    @SuppressWarnings("unused")
    public void setPlanetStateRepository(Repository<PlanetState> planetStateRepository) {
        this.planetStateRepository = planetStateRepository;
    }

    @SuppressWarnings("unused")
    public void setWeatherConditionRepository(WeatherConditionRepository weatherConditionRepository) {
        this.weatherConditionRepository = weatherConditionRepository;
    }
}
