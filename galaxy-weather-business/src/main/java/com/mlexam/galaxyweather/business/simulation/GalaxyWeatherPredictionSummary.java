package com.mlexam.galaxyweather.business.simulation;

import java.util.List;

public class GalaxyWeatherPredictionSummary {

    private int totalPredictedDays;
    private int dryDaysCount;
    private int rainyDaysCount;
    private int optimalWeatherDaysCount;
    private List<Integer> topRainyDays;

    public GalaxyWeatherPredictionSummary(int totalPredictedDays, int dryDaysCount, int rainyDaysCount, int optimalWeatherDaysCount, List<Integer> topRainyDays) {
        this.totalPredictedDays = totalPredictedDays;
        this.dryDaysCount = dryDaysCount;
        this.rainyDaysCount = rainyDaysCount;
        this.optimalWeatherDaysCount = optimalWeatherDaysCount;
        this.topRainyDays = topRainyDays;
    }

    public int getDryDaysCount() {
        return dryDaysCount;
    }

    public int getRainyDaysCount() {
        return rainyDaysCount;
    }

    public int getOptimalWeatherDaysCount() {
        return optimalWeatherDaysCount;
    }

    public List<Integer> getTopRainyDays() {
        return topRainyDays;
    }

    public int getTotalPredictedDays() {
        return totalPredictedDays;
    }
}
