package com.mlexam.galaxyweather.business.meteorology;

public enum WeatherKind {
    RAIN, OPTIMAL, UNDETERMINED, DRY
}
