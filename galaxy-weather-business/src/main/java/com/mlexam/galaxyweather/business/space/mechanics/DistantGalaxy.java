package com.mlexam.galaxyweather.business.space.mechanics;

public class DistantGalaxy {

    public static final double BETASOIDE_INITIAL_DEGREES = 90d;
    public static final double FERENGI_INITIAL_DEGREES = 90d;
    public static final double VULCANO_INITIAL_DEGREES = 90d;

    private MovablePlanet betasoidePlanet;
    private MovablePlanet ferengiPlanet;
    private MovablePlanet vulcanoPlanet;

    public DistantGalaxy(double betasoideCurrentDegrees, double ferengiCurrentDegrees, double vulcanoCurrentDegrees) {
        this.betasoidePlanet = new MovablePlanet(2000d, betasoideCurrentDegrees, -3d);
        this.ferengiPlanet = new MovablePlanet(500d, ferengiCurrentDegrees, -1d);
        this.vulcanoPlanet = new MovablePlanet(1000d, vulcanoCurrentDegrees, 5d);
    }

    public MovablePlanet getVulcanoPlanet() {
        return vulcanoPlanet;
    }

    public MovablePlanet getFerengiPlanet() {
        return ferengiPlanet;
    }

    public MovablePlanet getBetasoidePlanet() {
        return betasoidePlanet;
    }
}
