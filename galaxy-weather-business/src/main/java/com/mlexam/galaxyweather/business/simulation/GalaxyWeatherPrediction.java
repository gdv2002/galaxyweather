package com.mlexam.galaxyweather.business.simulation;

import java.math.BigDecimal;
import java.util.List;

public class GalaxyWeatherPrediction {

    private List<DayPredictionDetail> dayPredictionDetails;
    private int dryDaysCount;
    private int rainyDaysCount;
    private int optimalWeatherDaysCount;
    private List<Integer> topRainyDays;
    private BigDecimal maxRainyDayPerimeter;

    public GalaxyWeatherPrediction(List<DayPredictionDetail> predictionDetails, int dryDaysCount, int rainyDaysCount, int optimalWeatherDaysCount, List<Integer> topRainyDays, BigDecimal maxRainyDayPerimeter) {
        dayPredictionDetails = predictionDetails;
        this.dryDaysCount = dryDaysCount;
        this.rainyDaysCount = rainyDaysCount;
        this.optimalWeatherDaysCount = optimalWeatherDaysCount;
        this.topRainyDays = topRainyDays;
        this.maxRainyDayPerimeter = maxRainyDayPerimeter;
    }

    public List<DayPredictionDetail> getDayPredictionDetails() {
        return dayPredictionDetails;
    }

    public int getDryDaysCount() {
        return dryDaysCount;
    }

    public int getRainyDaysCount() {
        return rainyDaysCount;
    }

    public int getOptimalWeatherDaysCount() {
        return optimalWeatherDaysCount;
    }

    public List<Integer> getTopRainyDays() {
        return topRainyDays;
    }

    public BigDecimal getMaxRainyDayPerimeter() {
        return maxRainyDayPerimeter;
    }
}
