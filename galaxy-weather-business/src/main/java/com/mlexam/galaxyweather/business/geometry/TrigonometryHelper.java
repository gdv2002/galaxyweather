package com.mlexam.galaxyweather.business.geometry;

import java.math.BigDecimal;

public class TrigonometryHelper {

    public Point getPoint(double radius, double angleInDegrees) {
        double radians = Math.toRadians(angleInDegrees);
        return new Point(roundTo2Decimals(radius * Math.cos(radians)), roundTo2Decimals(radius * Math.sin(radians)));
    }

    private double roundTo2Decimals(double input) {
        return Math.round(input * 100d) / 100d;
    }

    public TriangleShapeInfo analyzeTriangle(Point vertexA, Point vertexB, Point vertexC) {

        // El metodo solo analiza casos donde los tres vertices sean diferentes
        if (vertexA.equals(vertexB))
            throw new IllegalArgumentException("Vertex B must be different than vertex A");
        if (vertexA.equals(vertexC))
            throw new IllegalArgumentException("Vertex C must be different than vertex A");
        if (vertexB.equals(vertexC))
            throw new IllegalArgumentException("Vertex C must be different than vertex B");

        BigDecimal area = calculateArea(vertexA, vertexB, vertexC);
        BigDecimal perimeter = (!area.equals(BigDecimal.ZERO)) ? calculatePerimeter(vertexA, vertexB, vertexC) : BigDecimal.ZERO;
        boolean containsOrigin;
        if (area.equals(BigDecimal.ZERO))
            containsOrigin = collinearPointsContainsOrigin(vertexA, vertexB, vertexC);
        else
            containsOrigin = triangleContainsPoint(vertexA, vertexB, vertexC, new Point(0, 0));

        return new TriangleShapeInfo(containsOrigin, perimeter, area);
    }

    private boolean triangleContainsPoint(Point v1, Point v2, Point v3, Point target) {

        // Barycentric coordinate system
        double alpha = ((v2.getY() - v3.getY()) * (target.getX() - v3.getX()) + (v3.getX() - v2.getX()) * (target.getY() - v3.getY())) /
                ((v2.getY() - v3.getY()) * (v1.getX() - v3.getX()) + (v3.getX() - v2.getX()) * (v1.getY() - v3.getY()));
        if (alpha < 0d)
            return false;

        double beta = ((v3.getY() - v1.getY()) * (target.getX() - v3.getX()) + (v1.getX() - v3.getX()) * (target.getY() - v3.getY())) /
                ((v2.getY() - v3.getY()) * (v1.getX() - v3.getX()) + (v3.getX() - v2.getX()) * (v1.getY() - v3.getY()));
        if (beta < 0d)
            return false;

        double gamma = 1d - alpha - beta;

        return (gamma >= 0);
    }

    private boolean collinearPointsContainsOrigin(Point vA, Point vB, Point vC) {
        Point origin = new Point(0, 0);

        return vA.equals(origin) || vB.equals(origin) || areCollinear(vA, vB, origin);
    }

    private boolean areCollinear(Point vA, Point vB, Point vC) {
        return calculateArea(vA, vB, vC).equals(BigDecimal.ZERO);
    }

    private BigDecimal calculatePerimeter(Point vertexA, Point vertexB, Point vertexC) {
        BigDecimal distanceFromAToB = vertexA.distance(vertexB);
        BigDecimal distanceFromAToC = vertexA.distance(vertexC);
        BigDecimal distanceFromBToC = vertexB.distance(vertexC);

        return distanceFromAToB.add(distanceFromAToC).add(distanceFromBToC);
    }

    private BigDecimal calculateArea(Point p1, Point p2, Point p3) {
        return new BigDecimal(((p3.getX() * p1.getY()) - (p1.getX() * p3.getY()) + (p2.getX() * p3.getY()) - (p3.getX() * p2.getY()) + (p1.getX() * p2.getY()) - (p2.getX() * p1.getY())) / 2d);
    }
}
