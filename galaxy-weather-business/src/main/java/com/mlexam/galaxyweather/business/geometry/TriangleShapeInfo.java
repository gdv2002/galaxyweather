package com.mlexam.galaxyweather.business.geometry;

import java.math.BigDecimal;

public class TriangleShapeInfo {

    private final boolean originContained;
    private final BigDecimal perimeter;
    private final BigDecimal area;

    public TriangleShapeInfo(boolean originContained, BigDecimal perimeter, BigDecimal area) {
        this.originContained = originContained;
        this.perimeter = perimeter;
        this.area = area;
    }

    public boolean isOriginContained() {
        return originContained;
    }

    public BigDecimal getPerimeter() {
        return perimeter;
    }

    public BigDecimal getArea() {
        return area;
    }
}
