package com.mlexam.galaxyweather.business.math;

import java.math.BigDecimal;

public class NumberHelper {

    public static double safelyExtractDouble(BigDecimal value, double defaultValue) {
        if (value == null)
            return defaultValue;
        return value.doubleValue();
    }

    public static boolean areEqual(BigDecimal value1, BigDecimal value2, BigDecimal tolerance) {
        BigDecimal result = value1.subtract(value2);
        return result.abs().compareTo(tolerance) <= 0;
    }

    public static BigDecimal max(BigDecimal value, BigDecimal... values) {
        BigDecimal max = value;
        for (BigDecimal otherValue : values) {
            if (max == null || (otherValue != null && otherValue.compareTo(max) > 0)) {
                max = otherValue;
            }
        }
        return max;
    }

    public static BigDecimal min(BigDecimal value, BigDecimal... values) {
        BigDecimal min = value;
        for (BigDecimal otherValue : values) {
            if (min == null || (otherValue != null && otherValue.compareTo(min) < 0)) {
                min = otherValue;
            }
        }
        return min;
    }
}
