package com.mlexam.galaxyweather.web.infrastructure.ioc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
@EnableWebMvc
@Configuration
public class CustomWebMvcConfigurer extends WebMvcConfigurerAdapter {

    @SuppressWarnings("unused")
    private final Logger logger = LoggerFactory.getLogger(CustomWebMvcConfigurer.class);

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter();
        jacksonConverter.setPrefixJson(false);
        jacksonConverter.setSupportedMediaTypes(buildJacksonConverterSupportedMediaTypes());
        jacksonConverter.setObjectMapper(buildJacksonConverterObjectMapper());

        converters.add(jacksonConverter);
    }

    private List<MediaType> buildJacksonConverterSupportedMediaTypes() {
        List<MediaType> supportedMediaTypes = new ArrayList<>();
        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
        return supportedMediaTypes;
    }

    private ObjectMapper buildJacksonConverterObjectMapper() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.modules(new JodaModule());
        return builder.build();
    }
}
