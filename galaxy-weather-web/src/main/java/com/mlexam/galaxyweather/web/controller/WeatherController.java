package com.mlexam.galaxyweather.web.controller;

import com.mlexam.galaxyweather.business.meteorology.MeteorologyService;
import com.mlexam.galaxyweather.business.meteorology.PredictionSummary;
import com.mlexam.galaxyweather.business.meteorology.WeatherKind;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/weather")
public class WeatherController {

    @SuppressWarnings("unused")
    private final Logger logger = LoggerFactory.getLogger(WeatherController.class);

    @Autowired
    private MeteorologyService meteorologyService;

    @RequestMapping(value = "/summary", method = RequestMethod.GET)
    public ResponseEntity<PredictionSummary> summary() {
        logger.info("Requested summary");
        return new ResponseEntity<>(meteorologyService.predictNextTenYears(), HttpStatus.OK);
    }

    @RequestMapping(value = "/day/{day}", method = RequestMethod.GET)
    public ResponseEntity<SingleDayResponseDto> singleDay(@PathVariable int day) {
        logger.info("Requested single day {}", day);
        return new ResponseEntity<>(new SingleDayResponseDto(meteorologyService.getSingleDayPrediction(day), day), HttpStatus.OK);
    }

    @SuppressWarnings("unused")
    public void setMeteorologyService(MeteorologyService meteorologyService) {
        this.meteorologyService = meteorologyService;
    }


    public class SingleDayResponseDto {

        private final WeatherKind weatherKind;
        private final int day;

        public SingleDayResponseDto(WeatherKind weatherKind, int day) {

            this.weatherKind = weatherKind;
            this.day = day;
        }

        public WeatherKind getWeatherKind() {
            return weatherKind;
        }

        public int getDay() {
            return day;
        }
    }
}
