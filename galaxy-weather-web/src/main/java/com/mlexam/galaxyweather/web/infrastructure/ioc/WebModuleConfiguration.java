package com.mlexam.galaxyweather.web.infrastructure.ioc;

import com.mlexam.galaxyweather.web.controller.AdminController;
import com.mlexam.galaxyweather.web.controller.WeatherController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SuppressWarnings("unused")
@Configuration
public class WebModuleConfiguration {

    @Bean
    public WeatherController weatherController() {
        return new WeatherController();
    }

    @Bean
    public AdminController adminController() {
        return new AdminController();
    }
}
