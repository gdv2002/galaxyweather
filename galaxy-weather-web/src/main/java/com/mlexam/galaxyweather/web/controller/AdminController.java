package com.mlexam.galaxyweather.web.controller;

import com.mlexam.galaxyweather.business.infrastructure.ioc.SystemInitialization;
import com.mlexam.galaxyweather.business.meteorology.MeteorologyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @SuppressWarnings("unused")
    private final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private MeteorologyService meteorologyService;

    @Autowired
    private SystemInitialization systemInitialization;

//    @RequestMapping(value = "/process-predictions", method = RequestMethod.POST)
    @RequestMapping(value = "/process-predictions", method = RequestMethod.GET)   // ejecutado via cronjob de appengine
    public ResponseEntity processPredictions() {
        logger.info("Process predictions requested");
        meteorologyService.processNextTenYearsPrediction();
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/initialize", method = RequestMethod.POST)
    public ResponseEntity initialize() {
        logger.info("system initialization requested");
        systemInitialization.setUp();
        return new ResponseEntity(HttpStatus.OK);
    }

    @SuppressWarnings("unused")
    public void setMeteorologyService(MeteorologyService meteorologyService) {
        this.meteorologyService = meteorologyService;
    }

    @SuppressWarnings("unused")
    public void setSystemInitialization(SystemInitialization systemInitialization) {
        this.systemInitialization = systemInitialization;
    }
}
