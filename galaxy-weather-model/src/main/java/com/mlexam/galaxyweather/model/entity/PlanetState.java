package com.mlexam.galaxyweather.model.entity;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class PlanetState extends BaseEntity {

    @Index
    private int day;
    private double angle;

    @Parent
    private Ref<Planet> planet;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public Planet getPlanet() {
        return planet.get();
    }

    public void setPlanet(Planet planet) {
        this.planet = Ref.create(planet);
    }
}
