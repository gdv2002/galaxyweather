package com.mlexam.galaxyweather.model.entity;

import com.googlecode.objectify.annotation.Id;

import java.util.UUID;

public abstract class BaseEntity {

    @Id
    private String id;

    public void assignNewRandomId() {
        setId(UUID.randomUUID().toString());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
