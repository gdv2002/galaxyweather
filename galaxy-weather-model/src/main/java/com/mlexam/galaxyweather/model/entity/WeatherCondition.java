package com.mlexam.galaxyweather.model.entity;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class WeatherCondition extends BaseEntity {

    @Index
    private int day;

    @Index
    private WeatherKind weatherKind;

    @Index
    private double perimeter;

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    @Parent
    private Key parent;   // Para poder asignar un parent ficticio compartido a todas las WeatherCondition

    public void assignSharedParent(String kind, String key) {
        parent = KeyFactory.createKey(kind, key);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public WeatherKind getWeatherKind() {
        return weatherKind;
    }

    public void setWeatherKind(WeatherKind weatherKind) {
        this.weatherKind = weatherKind;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }
}
