package com.mlexam.galaxyweather.model.entity;

public enum WeatherKind {
    RAIN, OPTIMAL, UNDETERMINED, DRY
}
