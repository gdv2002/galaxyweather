package com.mlexam.galaxyweather.model.entity;

import com.googlecode.objectify.annotation.Entity;

import java.util.Date;

@Entity
public class SystemDescription extends BaseEntity {

    public static final String UNIQUE_ID = "sysdescript";

    private Date initialDateInUtc;
    private double maxKnownRainyDayPerimeter;

    @Override
    public void setId(String id) {
        super.setId(UNIQUE_ID);
    }

    public Date getInitialDateInUtc() {
        return initialDateInUtc;
    }

    public void setInitialDateInUtc(Date initialDateInUtc) {
        this.initialDateInUtc = initialDateInUtc;
    }

    public double getMaxKnownRainyDayPerimeter() {
        return maxKnownRainyDayPerimeter;
    }

    public void setMaxKnownRainyDayPerimeter(double maxKnownRainyDayPerimeter) {
        this.maxKnownRainyDayPerimeter = maxKnownRainyDayPerimeter;
    }
}
